import * as vscode from 'vscode';
import { GitExtension, Repository } from './api/git';

interface RepositoryState {
  repository: Repository;
  commitMessage: string;
  disposable?: vscode.Disposable;
}

const extensionSettingsSection = 'gitCommitMessageTemplate';
let pattern: RegExp | undefined;
let template: string | undefined;

export function activate(context: vscode.ExtensionContext) {
  const gitExtension = vscode.extensions.getExtension<GitExtension>('vscode.git')?.exports;
  const git = gitExtension?.getAPI(1);
  let repositoryStates: RepositoryState[] = [];

  if (!git) {
    vscode.window.showErrorMessage('Extension vscode.git (api version 1) required but not found.');
    return;
  }

  updateConfigVars();

  if (!pattern || !template) {
    vscode.window.showInformationMessage('Pattern or template setting is not set', 'Open Settings')
      .then(() => vscode.commands.executeCommand('workbench.action.openSettings', extensionSettingsSection));
  }

  vscode.workspace.onDidChangeConfiguration(() => {
    updateConfigVars();
    repositoryStates.forEach(state => updateMessage(state));
  });

  const gitDisposable = git.onDidChangeState(state => {
    repositoryStates.forEach(repositoryState => repositoryState.disposable?.dispose());
    repositoryStates = [];

    if (state !== 'initialized') {
      return;
    }

    repositoryStates = git.repositories.map(repository => {
      let message = getMessage(repository);

      if (typeof repository.inputBox.value === 'undefined' || repository.inputBox.value === '') {
        repository.inputBox.value = message;
      }

      const repositoryState: RepositoryState = {
        repository,
        commitMessage: message,
        disposable: undefined,
      };
      repositoryState.disposable = repository.state.onDidChange(() => updateMessage(repositoryState));

      context.subscriptions.push(repositoryState.disposable);

      return repositoryState;
    });
  });

  context.subscriptions.push(gitDisposable);
}

export function deactivate() { }

function updateConfigVars() {
  const config = vscode.workspace.getConfiguration(extensionSettingsSection);
  template = config.get<string>('template');
  const patternConfig = config.get<string>('pattern');
  if (patternConfig) {
    pattern = new RegExp(patternConfig);
  }
}

function updateMessage(state: RepositoryState) {
  const { repository, commitMessage } = state;
  const newMessage = getMessage(repository);

  if (typeof repository.inputBox.value === 'undefined' || repository.inputBox.value === '' || repository.inputBox.value === commitMessage) {
    repository.inputBox.value = newMessage;
    state.commitMessage = newMessage;
  }
}

function getMessage(repository: Repository) {
  const branchName = repository?.state.HEAD?.name;
  if (!branchName || !pattern || !template) {
    return '';
  }

  let message = template;
  const match = branchName.match(pattern);

  if (match) {
    match.forEach((value, index) => {
      if (index > 0) {
        message = message.replace(`$${index}`, value);
      }
    });
    message = message.replace(/\\n/g, '\n');
    return message;
  }

  return '';
}
