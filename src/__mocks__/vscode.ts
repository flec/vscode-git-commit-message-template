export interface Extensions {
  getExtension: jest.Mock;
}

export interface Window {
  showErrorMessage: jest.Mock;
  showInformationMessage: jest.Mock;
}

export interface Workspace {
  getConfiguration: jest.Mock;
  onDidChangeConfiguration: jest.Mock;
}

export const extensions: Extensions = {
  getExtension: jest.fn(),
};

export const window: Window = {
  showErrorMessage: jest.fn(),
  showInformationMessage: jest.fn(),
};

export const workspace: Workspace = {
  getConfiguration: jest.fn(),
  onDidChangeConfiguration: jest.fn(),
};
