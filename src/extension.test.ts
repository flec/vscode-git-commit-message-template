import { activate } from './extension';
import { getExtensionContextMock, getOnDidChangeConfigurationTrigger, initConfig, initGit } from './test-util/vscode';

test('template is empty on master', done => {
  const inputBox = { value: undefined };
  const repositories = [{
    inputBox,
    state: {
      ['HEAD']: { name: 'master' },
      onDidChange: jest.fn(),
    },
  }];

  initConfig();

  initGit(repositories, () => {
    expect(inputBox.value).toBe('');
    done();
  });

  activate(getExtensionContextMock());
});

test('template contains match on feature branch with expected pattern', done => {
  const inputBox = { value: undefined };
  const repositories = [{
    inputBox,
    state: {
      ['HEAD']: { name: 'feature/first-feature_JIRA-1' },
      onDidChange: jest.fn(),
    },
  }];

  initConfig();

  initGit(repositories, () => {
    expect(inputBox.value).toBe('\nJIRA-1');
    done();
  });

  activate(getExtensionContextMock());
});

test('input box is left untouched when content does not match previous template', done => {
  const inputBox = { value: 'my content' };
  const repositories = [{
    inputBox,
    state: {
      ['HEAD']: { name: 'feature/first-feature_JIRA-1' },
      onDidChange: jest.fn(),
    },
  }];

  initConfig();

  initGit(repositories, () => {
    expect(inputBox.value).toBe('my content');
    done();
  });

  activate(getExtensionContextMock());
});

test('template is adapted on branch change', done => {
  const inputBox = { value: undefined };
  const gitHead = { name: 'feature/first-feature_JIRA-1' };
  const repositories = [{
    inputBox,
    state: {
      ['HEAD']: gitHead,
      onDidChange: jest.fn().mockImplementation(callback => {
        gitHead.name = 'feature/second-feature_JIRA-2';
        setTimeout(() => {
          callback();
          expect(inputBox.value).toBe('\nJIRA-2');
          done();
        }, 2);
      }),
    },
  }];

  initConfig();

  initGit(repositories, () => {
    expect(inputBox.value).toBe('\nJIRA-1');
  });

  activate(getExtensionContextMock());
});

test('template is adapted on template config change', done => {
  const inputBox = { value: undefined };
  const gitHead = { name: 'feature/first-feature_JIRA-1' };
  const repositories = [{
    inputBox,
    state: {
      ['HEAD']: gitHead,
      onDidChange: jest.fn(),
    },
  }];

  initConfig(undefined, ['$1 ', '\\n$1']);
  const changeConfig = getOnDidChangeConfigurationTrigger();

  initGit(repositories, () => {
    expect(inputBox.value).toBe('JIRA-1 ');
    changeConfig();
    expect(inputBox.value).toBe('\nJIRA-1');
    done();
  });

  activate(getExtensionContextMock());
});

test('template is adapted on pattern config change', done => {
  const inputBox = { value: undefined };
  const gitHead = { name: 'feature/tenth-feature_JIRA-10' };
  const repositories = [{
    inputBox,
    state: {
      ['HEAD']: gitHead,
      onDidChange: jest.fn(),
    },
  }];

  initConfig(['([A-Z]+-\\d)', '([A-Z]+-\\d+)']);
  const changeConfig = getOnDidChangeConfigurationTrigger();

  initGit(repositories, () => {
    expect(inputBox.value).toBe('\nJIRA-1');
    changeConfig();
    expect(inputBox.value).toBe('\nJIRA-10');
    done();
  });

  activate(getExtensionContextMock());
});

test('template is not adapted when custom', done => {
  const changeConfig = getOnDidChangeConfigurationTrigger();
  const inputBox = { value: 'my custom input' };
  const gitHead = { name: 'feature/first-feature_JIRA-1' };
  const repositories = [{
    inputBox,
    state: {
      ['HEAD']: gitHead,
      onDidChange: jest.fn().mockImplementation(callback => {
        gitHead.name = 'feature/second-feature_JIRA-2';
        setTimeout(() => {
          callback();
          expect(inputBox.value).toBe('my custom input');
          changeConfig();
          expect(inputBox.value).toBe('my custom input');
          done();
        }, 2);
      }),
    },
  }];

  initConfig(['([A-Z]+-\\d)', '([A-Z]+-\\d+)']);

  initGit(repositories, () => {
    expect(inputBox.value).toBe('my custom input');
  });

  activate(getExtensionContextMock());
});
