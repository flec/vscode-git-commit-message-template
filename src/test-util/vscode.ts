import type { ExtensionContext } from 'vscode';
import * as vscode from '../__mocks__/vscode';

export function initGit(repositories: unknown[], onDidChangeStateComplete: () => void) {
  vscode.extensions.getExtension.mockImplementation((extensionId: string) => {
    if (extensionId === 'vscode.git') {
      const getAPI = jest.fn().mockImplementation((version: number) => {
        if (version === 1) {
          return {
            onDidChangeState: jest.fn().mockImplementation(callback => {
              callback('initialized');
              onDidChangeStateComplete();
            }),
            repositories,
          };
        }
      });
      return { exports: { getAPI } };
    }
  });
}

export function initConfig(patterns?: string[], templates?: string[]) {
  vscode.workspace.getConfiguration.mockImplementation((section: string) => {
    if (section === 'gitCommitMessageTemplate') {
      return {
        get: jest.fn().mockImplementation((subSection: string) => {
          if (subSection === 'pattern') {
            return patterns ? patterns.shift() : '([A-Z]+-\\d+)';
          }
          if (subSection === 'template') {
            return templates ? templates.shift() : '\\n$1';
          }
        })
      };
    }
  });
}

export function getOnDidChangeConfigurationTrigger() {
  let callback = () => { throw new Error("onDidChangeConfiguration not configured"); };
  vscode.workspace.onDidChangeConfiguration.mockImplementation(cb => {
    callback = cb;
  });
  return () => callback();
}

export function getExtensionContextMock(): ExtensionContext {
  return {
    subscriptions: {
      push: jest.fn() as ExtensionContext['subscriptions']['push'],
    },
  } as ExtensionContext;
}
