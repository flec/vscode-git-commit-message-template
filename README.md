# Git Commit Message Template for Visual Studio Code

This extension fills the commit message field with a template that can include parts that are extracted from the branch name (eg. Jira Issue).

## Features

![preview](images/preview.gif)

## Installation
```
code --install-extension git-commit-message-template-<VERSION>.vsix
```

## Requirements

This extension depends on `vscode.git` which is shipped with vscode.

## Extension Settings

This extension contributes the following settings:

* `gitCommitMessageTemplate.pattern`: A regular expression to extract tokens from branch name
* `gitCommitMessageTemplate.template`: Template for you commit message. Use backreferences (eg. `$1`) to insert matches from your pattern

## Building
```
npm ci
npm run package
```

## Known Issues

Currently there are no known issues.

## Release Notes

### 0.1.0

Initial release of Git Commit Message Template


